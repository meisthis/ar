package com.example.hackathn;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.google.ar.sceneform.samples.augmentedimage.R;

public class MainActivity extends AppCompatActivity {


    protected static final String TAG = "RangingActivity";
    static String key="lol";

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.webview);

        WebView web = findViewById(R.id.web_view);
        WebSettings webSettings = web.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);

        webSettings.setAllowFileAccessFromFileURLs(true);
        webSettings.setAllowUniversalAccessFromFileURLs(true);
        
        webSettings.setAppCacheEnabled(true);
        webSettings.setDatabaseEnabled(true);



        web.loadUrl("https://pointt-d5bcb.firebaseapp.com");


        web.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                view.evaluateJavascript("javascript:JSON.parse(window.localStorage.getItem('userId'));", new ValueCallback<String>() {
                    @Override
                    public void onReceiveValue(String value) {
                        Log.d("checkusercoke",value);
                        key=value;
                        Log.d("coke",key);
                    }
                });
            }

            String curr = web.getUrl();

            @Override
            public void onLoadResource(WebView view, String url) {
                super.onLoadResource(view, url);
                Log.d(TAG, "ckewo" + web.getUrl());
                curr = web.getUrl();

                if (curr.equals("https://pointt-d5bcb.firebaseapp.com/point-camera")) {
                    Intent intent = new Intent(MainActivity.this, Augmented.class);
                    startActivity(intent);
                }
            }


        });


        webSettings.getDomStorageEnabled();

        web.evaluateJavascript("localStorage.getItem('userInfo')",null);


        }


    public static String getKey() {
        return key;
    }

}






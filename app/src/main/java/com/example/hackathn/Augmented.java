package com.example.hackathn;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.hackathn.augmentedReality.ActivityNode;
import com.example.hackathn.model.ARToken;
import com.example.hackathn.model.ArTransaction;
import com.example.hackathn.routing.API;
import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Frame;
import com.google.ar.core.TrackingState;
import com.google.ar.sceneform.FrameTime;
import com.google.ar.sceneform.samples.augmentedimage.R;
import com.google.ar.sceneform.ux.ArFragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Augmented extends AppCompatActivity {

    private ArFragment arFragment;
    private ImageView fitToScanView;

    // Augmented image and its associated center pose anchor, keyed by the augmented image in
    // the database.
    private final Map<AugmentedImage, ActivityNode> augmentedImageMap = new HashMap<>();

    ArrayList<ArTransaction> transactionModelArrayList ;

    private ProgressBar pBar;
    private FloatingActionButton animationButton;
    private String TAG = "TOKEN";

    public String getUserId() {
        return userId.trim().replace("\"","");
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    boolean checking;
    String userId;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        getTransaction();
        arFragment = (ArFragment) getSupportFragmentManager().findFragmentById(R.id.ux_fragment);

        fitToScanView = findViewById(R.id.image_view_fit_to_scan);
        arFragment.getArSceneView().getScene().addOnUpdateListener(this::onUpdateFrame);
        setUserId(MainActivity.getKey());
        pBar = findViewById(R.id.progressBar);
//        Log.d("coke",MainActivity.getKey());


        Log.d("coke",userId);
        animationButton = findViewById(R.id.animate);

        animationButton.setEnabled(true);
        animationButton.setOnClickListener(this::popUp);


    }

    private void popUp(View view) {


//        getResponse();
        Toast toast = Toast.makeText(this, "NO AR DETECTED", Toast.LENGTH_SHORT);
        Log.d(
                TAG,
                String.format(
                        "NO AR DETECTED"));
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (augmentedImageMap.isEmpty()) {
            fitToScanView.setVisibility(View.VISIBLE);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    private void onUpdateFrame(FrameTime frameTime) {
        Frame frame = arFragment.getArSceneView().getArFrame();

        // If there is no frame or ARCore is not tracking yet, just return.
        if (frame == null || frame.getCamera().getTrackingState() != TrackingState.TRACKING) {
            return;
        }
//        getResponse();
//        getTransaction();


        Collection<AugmentedImage> updatedAugmentedImages =
                frame.getUpdatedTrackables(AugmentedImage.class);
        for (AugmentedImage augmentedImage : updatedAugmentedImages) {
            switch (augmentedImage.getTrackingState()) {
                case PAUSED:
                    // When an image is in PAUSED state, but the camera is not PAUSED, it has been detected,
                    // but not yet tracked.
                    //jangan snackbar tapi loading

                    Log.d("loading","loadingcheck");
                    pBar.setVisibility(View.VISIBLE);


                    break;

                case TRACKING:
                    // Have to switch to UI Thread to update View.
                    fitToScanView.setVisibility(View.GONE);
                    pBar.setVisibility(View.GONE);

                    // Create a new anchor for newly found images.
                    if (!augmentedImageMap.containsKey(augmentedImage)) {
                        ActivityNode node = new ActivityNode(this);
                        node.setImage(augmentedImage);
                        augmentedImageMap.put(augmentedImage, node);
                        arFragment.getArSceneView().getScene().addChild(node);
                        animationButton = findViewById(R.id.animate);

                        animationButton.setEnabled(true);
                        animationButton.setOnClickListener(this::popUpTRUE);

                    }

                    break;

                case STOPPED:
                    augmentedImageMap.remove(augmentedImage);
                    break;
            }
        }
    }

    public void getTransaction(){
        Log.i(TAG,"KONTOL");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://hackathon-api.azurewebsites.net")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        API api = retrofit.create(API.class);
        Call<List<ArTransaction>> call = api.getTransaction();
        call.enqueue(new Callback<List<ArTransaction>>() {
            //            @RequiresApi(api = Build.VERSION_CODES.O)
//
//            Boolean newest=false;
//            String wkwk="testing";
            @Override
            public void onResponse(Call<List<ArTransaction>> call, Response<List<ArTransaction>> response) {
//                Log.i("Responsestring", response.body().get(0).getArTransactionId());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i("onSuccess", response.body().toString());
//                        Log.i(TAG,"wkwkwk"+response.body().get(1).getArTransactionId());

                        List<ArTransaction> koko = response.body();
                        for(int i=0;i<koko.size();i++){
                            Log.d("StringID",koko.get(i).getArTransactionId());

                            if(koko.get(i).getUser().getUserId().equalsIgnoreCase(getUserId())){
                                Log.d("userId",koko.get(i).getUser().getUserId());

                                if(koko.get(i).getNewest().equals(true)){
                                    Log.d("falsetruenye",koko.get(i).getNewest().toString());
                                    checking=true;
                                }

                            }
                            else { Log.d(TAG,"noID");}
                        }


//                        transactionModelArrayList = new ArrayList<>(Arrays.asList(koko.get().));
//                        for (int i = 0; i < transactionModelArrayList.size(); i++) {
//                             checking = transactionModelArrayList.get(i).getNewest();
//                             testing = transactionModelArrayList.get(i).getArTransactionId();
//                        }
//                        Log.d("testinga",testing);
//                            setChecking(newest);
//                            setTesting(testing);

//                            Log.d("wkwk",wkwk);
                    }




//                        String jsonResponse = response.body().toString();
//                        ArrayList<ArTransaction> data = new ArrayList<>(Arrays.asList(jsonResponse.getAndroid()));
//                        adapter = new DataAdapter(data);
//                        recyclerView.setAdapter(adapter);
//                            writeTransaction(jsonResponse);
//                    } else {
//                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
//                    }
                }
            }

            @Override
            public void onFailure(Call<List<ArTransaction>> call, Throwable t) {
                Log.i("wkaakw",t.getLocalizedMessage());

            }
        });



    }



    private void Post(final ARToken point) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://hackathon-api.azurewebsites.net")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        API requestInterface = retrofit.create(API.class);
        Call<ARToken> call = requestInterface.pospoint(point);

        call.enqueue(new Callback<ARToken>() {
            @Override
            public void onResponse(Call<ARToken> call, Response<ARToken> response) {
//        loadJSON();
//        finish();

//                isPosted = true;
                Log.i(TAG,point.getLocationId());
//        Log.i(TAG4,"Here Bro"+point.getARToken());
            }

            @Override
            public void onFailure(Call<ARToken> call, Throwable t) {
            }
        });
    }


    private void popUpTRUE(View view) {

        AlertDialog.Builder alert = new AlertDialog.Builder(Augmented.this);

//        getTransaction();
//        getResponse();
//        Log.d("checkmethod", String.valueOf(methodget.isWaitOneDay()));
        String asd="kont";
        if(checking==true){
            asd="true";
        }
        Log.d("BANGSTA",asd);
        if(checking==false) {
            MediaPlayer ring = MediaPlayer.create(Augmented.this, R.raw.victor);
            ring.start();

            alert.setTitle("Congratulations");
            alert.setMessage("YOU GOT 1 TOKEN!!").
                    setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ARToken point = new ARToken("AL_00501", getUserId());
                            Log.d("checkpost",getUserId());
                            Post(point);

                            Augmented.this.finish();
                            ring.stop();
                            Intent intent = new Intent(Augmented.this, MainActivity.class);
                            startActivity(intent);
                        }
                    });
            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }
        else {
            MediaPlayer ring = MediaPlayer.create(Augmented.this, R.raw.sad);
            ring.start();


            alert.setTitle("Sorry");
            alert.setMessage("You've Already Claimed This Token").
                    setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ring.stop();
                            Augmented.this.finish();


                            Intent intent = new Intent(Augmented.this, MainActivity.class);
                            startActivity(intent);

                        }
                    });
            AlertDialog alertDialog = alert.create();
            alertDialog.show();

        }
    }

}

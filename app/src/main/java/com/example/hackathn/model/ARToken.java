package com.example.hackathn.model;

public class ARToken {
    private String arLocationId;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLocationId() {
        return arLocationId;
    }

    public void setLocationId(String locationId) {
        arLocationId   = locationId;
    }

    public ARToken(String locationId, String userId) {
        arLocationId  = locationId;
        this.userId = userId;
    }

    public ARToken(String locationId) {
        arLocationId = locationId;
    }
}

package com.example.hackathn.model;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ArTransaction {

    @SerializedName("arTransactionId")
    @Expose
    private String arTransactionId;
    @SerializedName("arTransactionTime")
    @Expose
    private List<Integer> arTransactionTime = null;
    @SerializedName("arLocation")
    @Expose
    private ArLocation arLocation;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("newest")
    @Expose
    private Boolean newest;

    public String getArTransactionId() {
        return arTransactionId;
    }

    public void setArTransactionId(String arTransactionId) {
        this.arTransactionId = arTransactionId;
    }

    public List<Integer> getArTransactionTime() {
        return arTransactionTime;
    }

    public void setArTransactionTime(List<Integer> arTransactionTime) {
        this.arTransactionTime = arTransactionTime;
    }

    public ArLocation getArLocation() {
        return arLocation;
    }

    public void setArLocation(ArLocation arLocation) {
        this.arLocation = arLocation;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getNewest() {
        return newest;
    }

    public void setNewest(Boolean newest) {
        this.newest = newest;
    }
}
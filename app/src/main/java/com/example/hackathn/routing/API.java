package com.example.hackathn.routing;

import com.example.hackathn.model.ARToken;
import com.example.hackathn.model.ArTransaction;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface API {

    @POST("api/v1/ar-transaction/save-ar-transaction")
    Call<ARToken> pospoint(@Body ARToken arToken);

    @GET("api/v1/ar-location/get-all-location")
    Call<String> getlocation();

    @GET("api/v1/ar-transaction/get-all-transaction")
    Call<List<ArTransaction>> getTransaction();
}

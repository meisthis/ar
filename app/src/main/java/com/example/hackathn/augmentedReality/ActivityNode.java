package com.example.hackathn.augmentedReality;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Button;

import com.google.ar.core.AugmentedImage;
import com.google.ar.core.Pose;
import com.google.ar.sceneform.AnchorNode;
import com.google.ar.sceneform.Node;
import com.google.ar.sceneform.math.Quaternion;
import com.google.ar.sceneform.math.Vector3;
import com.google.ar.sceneform.rendering.ModelRenderable;

import java.util.concurrent.CompletableFuture;

public class ActivityNode extends AnchorNode {
    private static final String TAG = "AugmentedImageNode";


    // The augmented image represented by this node.
    private AugmentedImage image;

    private static CompletableFuture<ModelRenderable> modelRenderableCompletableFuture;


    @RequiresApi(api = Build.VERSION_CODES.N)
    public ActivityNode(Context context) {
        // Upon construction, start loading the models for the corners of the frame.
        if (modelRenderableCompletableFuture == null) {
            modelRenderableCompletableFuture =
                    ModelRenderable.builder()
                            .setSource(context, Uri.parse("homecredit.sfb"))
                            .build();


        }
    }

    /**
     * Called when the AugmentedImage is detected and should be rendered. A Sceneform node tree is
     * created based on an Anchor created from the image. The corners are then positioned based on the
     * extents of the image. There is no need to worry about world coordinates since everything is
     * relative to the center of the image, which is the parent node of the corners.
     */
    @RequiresApi(api = Build.VERSION_CODES.N)
    @SuppressWarnings({"AndroidApiChecker", "FutureReturnValueIgnored"})
    public void setImage(AugmentedImage image) {
        this.image = image;

        // If any of the models are not loaded, then recurse when all are loaded.
        if (!modelRenderableCompletableFuture.isDone()) {
            CompletableFuture.allOf(modelRenderableCompletableFuture)
                    .thenAccept((Void aVoid) -> setImage(image))
                    .exceptionally(
                            throwable -> {
                                Log.e(TAG, "Exception loading", throwable);
                                return null;
                            });
        }

        // Set the anchor based on the center of the image.
        setAnchor(image.createAnchor(image.getCenterPose()));


        Node node= new Node();
        Pose pose = Pose.makeTranslation(0.0f
                * image.getExtentX(),0.0f,0.5f*image.getExtentZ());

        node.setParent(this);
        node.setLocalPosition(new Vector3(pose.tx(),pose.ty(),pose.tz()));
        node.setLocalRotation(new Quaternion(pose.qx(),pose.qy(),pose.qz(),pose.qw()));
        node.setRenderable(modelRenderableCompletableFuture.getNow(null));

    }
}
